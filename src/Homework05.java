class MainThread {
    static synchronized void print(String word, int time){
        if(word.equals("*") || word.equals("-")){
            for (int i=0; i<=35; i++){
                System.out.print(word);
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println();
        }
        else {
            for (int i=0; i<word.length(); i++){
                System.out.print(word.charAt(i));
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

class MyThread extends Thread{
    String word;
    int time;
    MyThread(String word, int time){
        this.word = word;
        this.time = time;
    }

    @Override
    public void run() {
        MainThread.print(word, time);
    }
}

public class Homework05 {
    public static void main(String []args){
        MyThread t1 = new MyThread("Hello KSHRD!\n", 250);
        MyThread t2 = new MyThread("*", 250);
        MyThread t3 = new MyThread("I will try my best to be here at HRD.\n", 250);
        MyThread t4 = new MyThread("-",250);
        MyThread t5 = new MyThread("Downloading..........", 250);
        MyThread t6 = new MyThread("Completed 100%!",10);

        try {
            t1.start();
            t1.join();
            t2.start();
            t2.join();
            t3.start();
            t3.join();
            t4.start();
            t4.join();
            t5.start();
            t5.join();
            t6.start();
            t6.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
